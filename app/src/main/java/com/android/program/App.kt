package com.android.program

import android.app.Application
import com.android.program.di.app_module
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(app_module)
        }
    }
}