package com.android.program.di

import com.android.program.data.repository.ProgramRepository
import com.android.program.data.repository.ProgramRepositoryImpl
import org.koin.dsl.module

val repository = module {

    factory<ProgramRepository> { ProgramRepositoryImpl(get()) }
}