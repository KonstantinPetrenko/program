package com.android.program.di

val app_module = listOf(
    viewModelModule,
    repository,
    dataSourceModule
)