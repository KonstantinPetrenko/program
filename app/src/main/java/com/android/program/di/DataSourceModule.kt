package com.android.program.di

import com.android.program.data.network.ApiService
import com.android.program.data.network.datasource.NetworkDataSource
import com.android.program.data.network.datasource.NetworkDataSourceImpl
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

const val BASE_URL = "https://oll.tv"
private const val CONNECT_TIMEOUT = 15L
private const val READ_TIMEOUT = 30L

val dataSourceModule = module {
    single { createOkHttpClient() }

    single { createApiRestService<ApiService>(get()) }

    single<NetworkDataSource> { NetworkDataSourceImpl(get()) }
}

private fun createOkHttpClient(): OkHttpClient {
    val okHttpBuilder = OkHttpClient.Builder()
        .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
        .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
        .writeTimeout(READ_TIMEOUT, TimeUnit.SECONDS)

    okHttpBuilder.addInterceptor(getHttpLoggingInterceptor())
    okHttpBuilder.addInterceptor { chain ->
        val requestBuilder = chain.request().newBuilder().apply {
            addHeader("Content-Type", "application/json")
        }
        chain.proceed(requestBuilder.build())
    }
    return okHttpBuilder.build()
}

private fun getHttpLoggingInterceptor(): HttpLoggingInterceptor {
    val logging = HttpLoggingInterceptor()
    logging.setLevel(HttpLoggingInterceptor.Level.BODY)
    return logging
}

inline fun <reified T> createApiRestService(okHttpClient: OkHttpClient): T {
    val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .client(okHttpClient)
        .build()

    return retrofit.create(T::class.java)
}