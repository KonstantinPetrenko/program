package com.android.program.di

import com.android.program.ui.program.ProgramViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel {ProgramViewModel(get())}
}