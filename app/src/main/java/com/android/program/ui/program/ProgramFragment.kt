package com.android.program.ui.program

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.android.program.R
import com.android.program.databinding.FragmentProgramBinding
import com.android.program.databinding.LayoutPreloaderBinding
import com.android.program.ui.program.adapter.ProgramAdapter
import com.android.program.ui.uimodel.ItemProgramUi
import com.android.program.utils.Constants
import com.android.program.utils.extensions.addPagination
import com.android.program.utils.extensions.observe
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProgramFragment : Fragment() {

    private var binding: FragmentProgramBinding? = null
    private val viewModel: ProgramViewModel by viewModel()
    private var loaderDialog: AlertDialog? = null
    private var programAdapter: ProgramAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(LayoutInflater.from(inflater.context),
                R.layout.fragment_program, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (viewModel.rawProgramList.isNullOrEmpty()) {
            viewModel.getProgram()
        } else {
            viewModel.programResult.value = viewModel.rawProgramList
            binding?.rvProgram?.layoutManager?.onRestoreInstanceState(
                    savedInstanceState?.getParcelable(
                            Constants.KEY_PROGRAM_LIST_POSITION
                    )
            )
        }
        initRecyclerView()
        setObservers()
    }

    private fun initRecyclerView() {
        binding?.rvProgram?.apply {
            programAdapter = ProgramAdapter(::navigateToDetailsProgram)
            adapter = programAdapter
        }
    }

    override fun onResume() {
        super.onResume()
        binding?.rvProgram?.addPagination(viewModel.isLoading) {
            viewModel.getProgram(it)
        }
    }

    private fun setObservers() {
        observe(viewModel.resultErrorMessage) { message ->
            showError(message)
        }
        observe(viewModel.isLoading) { loading ->
            val safeContext = context ?: return@observe
            loaderDialog?.dismiss()
            if (loading) {
                loaderDialog = createAndShowLoaderDialog(safeContext)
                return@observe
            }
            loaderDialog = null
        }
        observe(viewModel.programResult) {
            showProgram(it)
        }
    }

    private fun showError(message: String?) {
        val safeContext = context ?: return
        message?.let {
            AlertDialog.Builder(safeContext)
                    .setMessage(it)
                    .setPositiveButton(
                            R.string.btn_ok
                    ) { dialog, _ ->
                        dialog.cancel()
                    }
                    .show()
        }
    }

    private fun createAndShowLoaderDialog(context: Context): AlertDialog {

        val binding = DataBindingUtil.inflate<LayoutPreloaderBinding>(
                this.layoutInflater,
                R.layout.layout_preloader,
                null,
                false
        )

        val builder = AlertDialog.Builder(context, R.style.PreLoaderDialog)
        builder.setView(binding.root)

        val alert = builder.create()
        alert.setCanceledOnTouchOutside(false)
        alert.setCancelable(true)
        alert.show()
        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return alert
    }

    private fun showProgram(list: List<ItemProgramUi>) {
        programAdapter?.refreshItems(list)
    }

    private fun navigateToDetailsProgram(item: ItemProgramUi) {
        findNavController().navigate(ProgramFragmentDirections.actionProgramFragmentToDetailProgramFragment(item))
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(
                Constants.KEY_PROGRAM_LIST_POSITION,
                binding?.rvProgram?.layoutManager?.onSaveInstanceState()
        )
    }
}