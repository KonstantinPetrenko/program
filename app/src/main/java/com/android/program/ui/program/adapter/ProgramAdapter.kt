package com.android.program.ui.program.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.android.program.R
import com.android.program.ui.program.holder.ProgramHolder
import com.android.program.ui.uimodel.ItemProgramUi

class ProgramAdapter(private val programClickListener: ((ItemProgramUi) -> Unit)? = null)
    : RecyclerView.Adapter<ProgramHolder>() {

    private val items: ArrayList<ItemProgramUi> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProgramHolder {
        return ProgramHolder(
                DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        R.layout.item_program,
                        parent,
                        false
                ),
                programClickListener
        )
    }

    override fun onBindViewHolder(holder: ProgramHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    fun refreshItems(newList: List<ItemProgramUi>) {
        val diffResult = DiffUtil.calculateDiff(DiffUtilsProgram(items, newList))
        diffResult.dispatchUpdatesTo(this)
        items.clear()
        items.addAll(newList)
    }
}