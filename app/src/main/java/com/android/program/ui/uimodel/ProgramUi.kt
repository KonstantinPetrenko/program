package com.android.program.ui.uimodel

import android.os.Parcelable
import com.android.program.data.network.model.ProgramResponse
import kotlinx.parcelize.Parcelize

@Parcelize
data class ProgramUi(
        val items: List<ItemProgramUi>,
        val hasMore: Int,
        val itemsNumber: Int,
        val offset: Int,
        val total: Int
) : Parcelable {

    companion object {
        fun map(input: ProgramResponse?): ProgramUi {
            return ProgramUi(
                    items = extractItemsList(input?.items),
                    hasMore = input?.hasMore ?: 0,
                    itemsNumber = input?.itemsNumber ?: 0,
                    offset = input?.offset ?: 0,
                    total = input?.total?.toIntOrNull() ?: 0
            )
        }

        private fun extractItemsList(input: List<ProgramResponse.Item?>?): List<ItemProgramUi> {
            return input?.map {
                ItemProgramUi(
                        icon = it?.icon ?: "",
                        id = it?.id ?: 0L,
                        name = it?.name ?: "",
                        time = it?.start ?: ""
                )
            } ?: emptyList()
        }
    }
}

@Parcelize
data class ItemProgramUi(val icon: String, val id: Long, val name: String, val time: String) : Parcelable


