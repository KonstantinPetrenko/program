package com.android.program.ui.program

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.program.data.network.ResultWrapper
import com.android.program.data.repository.ProgramRepository
import com.android.program.ui.uimodel.ItemProgramUi
import com.android.program.utils.Constants
import com.android.program.utils.SingleLiveEvent
import kotlinx.coroutines.launch
import java.util.*

class ProgramViewModel(
        private val repository: ProgramRepository
) : ViewModel() {

    private val uuid = UUID.randomUUID().toString()
    val resultErrorMessage = SingleLiveEvent<String>()
    val isLoading = MutableLiveData<Boolean>()
    val programResult = SingleLiveEvent<List<ItemProgramUi>>()
    val rawProgramList = ArrayList<ItemProgramUi>()

    private var firstBorderId = 0L
    private var lastBorderId = 0L
    private var total = 0
    private var offset = 0
    private var more = 0

    fun getProgram(direction: Int = Constants.PAGINATION_DEFAULT) {
        when (direction) {
            Constants.PAGINATION_DIRECTION_UP -> {
                if (offset == 0) return
            }
            Constants.PAGINATION_DIRECTION_DOWN -> {
                if (more == 0) return
            }
        }

        val borderId = when (direction) {
            Constants.PAGINATION_DIRECTION_UP -> firstBorderId
            Constants.PAGINATION_DIRECTION_DOWN -> rawProgramList.last().id
            else -> 0
        }

        viewModelScope.launch {
            isLoading.postValue(true)
            when (val result = repository.getProgram(uuid, borderId, direction)) {
                is ResultWrapper.Success -> {
                    when (direction) {
                        Constants.PAGINATION_DEFAULT -> {
                            firstBorderId = result.data.items.first().id
                            lastBorderId = result.data.items.last().id
                            rawProgramList.addAll(result.data.items)
                        }
                        Constants.PAGINATION_DIRECTION_UP -> {
                            firstBorderId = result.data.items.first().id
                            rawProgramList.addAll(0, result.data.items)
                        }
                        Constants.PAGINATION_DIRECTION_DOWN -> {
                            lastBorderId = result.data.items.last().id
                            rawProgramList.addAll(result.data.items)
                        }
                    }
                    offset = result.data.offset
                    more = result.data.hasMore
                    total = result.data.total
                    programResult.postValue(rawProgramList)
                }
                is ResultWrapper.Error -> {
                    result.error.message?.let { resultErrorMessage.postValue(it) }
                }
            }
            isLoading.postValue(false)
        }
    }
}