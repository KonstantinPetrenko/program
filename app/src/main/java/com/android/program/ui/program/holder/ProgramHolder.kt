package com.android.program.ui.program.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.android.program.R
import com.android.program.databinding.ItemProgramBinding
import com.android.program.ui.uimodel.ItemProgramUi
import com.android.program.ui.uimodel.ProgramUi

class ProgramHolder(private val binding: ItemProgramBinding,
                    private val programClickListener: ((ItemProgramUi) -> Unit)?)
    : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: ItemProgramUi) {
        val clickListener = View.OnClickListener {
            when (it.id) {
                R.id.programItemContainer -> {
                    programClickListener?.invoke(item)
                }
            }
        }

        binding.item = item
        binding.clickListener = clickListener
        binding.executePendingBindings()
    }
}