package com.android.program.utils.extensions

import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData

    inline fun <T> Fragment.observe(liveData: LiveData<T>?, crossinline callback: (T) -> Unit) {
        liveData?.observe(this.viewLifecycleOwner) {
            callback.invoke(it)
        }
    }