package com.android.program.utils

object Constants {
    const val PAGINATION_LIMIT_FOR_PAGE = 40
    const val PAGINATION_DEFAULT = 0
    const val PAGINATION_DIRECTION_UP = -1
    const val PAGINATION_DIRECTION_DOWN = 1
    const val KEY_PROGRAM_LIST_POSITION = "key_program_list_position"
}