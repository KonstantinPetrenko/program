package com.android.program.utils.extensions

import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.program.utils.Constants

fun RecyclerView.addPagination(paginationLoadingState: MutableLiveData<Boolean>,
                               paginationInteractor: (Int) -> Unit) {

    addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            if (paginationLoadingState.value == true) return

            val layoutManager = recyclerView.layoutManager as? LinearLayoutManager ?: return
            val visibleItemCount: Int = layoutManager.childCount
            val totalItemCount = layoutManager.itemCount
            val firstVisibleItemPosition: Int = layoutManager.findFirstVisibleItemPosition()

            when {
                dy > 0 -> {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= Constants.PAGINATION_LIMIT_FOR_PAGE) {
                        paginationInteractor.invoke(Constants.PAGINATION_DIRECTION_DOWN)
                    }
                }
                else -> {
                    if (totalItemCount >= Constants.PAGINATION_LIMIT_FOR_PAGE && firstVisibleItemPosition == 0) {
                        paginationInteractor.invoke(Constants.PAGINATION_DIRECTION_UP)
                    }
                }
            }
        }
    })
}