package com.android.program.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

object BindingAdapter {

    @BindingAdapter("setImage")
    @JvmStatic
    fun setImage(imageView: ImageView, imageUrl: String?) {
        imageUrl?.let {
            Glide.with(imageView).load(it).into(imageView)
        }
    }
}