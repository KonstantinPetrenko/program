package com.android.program.data.network.datasource

import com.android.program.data.network.ResultWrapper
import com.android.program.data.network.model.ProgramResponse

interface NetworkDataSource {
    suspend fun getProgram(serialNumber: String, borderId: Long, direction: Int) : ResultWrapper<ProgramResponse>
}