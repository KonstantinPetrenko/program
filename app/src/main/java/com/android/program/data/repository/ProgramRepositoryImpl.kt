package com.android.program.data.repository

import com.android.program.data.network.ResultWrapper
import com.android.program.data.network.datasource.NetworkDataSource
import com.android.program.ui.uimodel.ProgramUi

class ProgramRepositoryImpl(
        private val networkDataSource: NetworkDataSource
) : ProgramRepository {
    override suspend fun getProgram(serialNumber: String, borderId: Long, direction: Int): ResultWrapper<ProgramUi> {
        return when (val result = networkDataSource.getProgram(serialNumber, borderId, direction)) {
            is ResultWrapper.Success -> {
                ResultWrapper.Success(data = ProgramUi.map(result.data))
            }
            is ResultWrapper.Error -> {
                result
            }
        }
    }
}