package com.android.program.data.network.model
import com.google.gson.annotations.SerializedName


data class ProgramResponse(
    @SerializedName("block_id")
    val blockId: String?,
    @SerializedName("block_title")
    val blockTitle: String?,
    @SerializedName("block_type")
    val blockType: String?,
    @SerializedName("category_name")
    val categoryName: String?,
    @SerializedName("first_now_index")
    val firstNowIndex: Int?,
    @SerializedName("hasMore")
    val hasMore: Int?,
    @SerializedName("items")
    val items: List<Item?>?,
    @SerializedName("items_number")
    val itemsNumber: Int?,
    @SerializedName("offset")
    val offset: Int?,
    @SerializedName("total")
    val total: String?
) {
    data class Item(
        @SerializedName("alias")
        val alias: String?,
        @SerializedName("channel_id")
        val channelId: Int?,
        @SerializedName("channel_name")
        val channelName: String?,
        @SerializedName("description")
        val description: String?,
        @SerializedName("dvr")
        val dvr: Int?,
        @SerializedName("FK_catalog")
        val fKCatalog: Int?,
        @SerializedName("icon")
        val icon: String?,
        @SerializedName("id")
        val id: Long?,
        @SerializedName("is_favorite")
        val isFavorite: Int?,
        @SerializedName("is_free")
        val isFree: Int?,
        @SerializedName("name")
        val name: String?,
        @SerializedName("now")
        val now: Int?,
        @SerializedName("start")
        val start: String?,
        @SerializedName("start_ts")
        val startTs: Int?,
        @SerializedName("stop")
        val stop: String?,
        @SerializedName("stop_ts")
        val stopTs: Int?,
        @SerializedName("subs")
        val subs: Subs?,
        @SerializedName("under_parental_protect")
        val underParentalProtect: Int?
    ) {
        data class Subs(
            @SerializedName("id")
            val id: Long?,
            @SerializedName("name")
            val name: String?,
            @SerializedName("price")
            val price: Int?,
            @SerializedName("subsId")
            val subsId: Long?,
            @SerializedName("type")
            val type: String?
        )
    }
}