package com.android.program.data.network

class ApiException(val responseCode: Int, val serverMessage: String?) : Exception(serverMessage)
