package com.android.program.data.network

import com.android.program.data.network.model.ProgramResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("/api/demo")
    suspend fun getProgram(
            @Query("serial_number") serialNumber: String,
            @Query("borderId") borderId: Long,
            @Query("direction") direction: Int
    ): Response<ProgramResponse>
}