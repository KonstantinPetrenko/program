package com.android.program.data.network

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

suspend fun <T> safeApiCall(
        dispatcher: CoroutineDispatcher = Dispatchers.IO,
        apiCall: suspend () -> Response<T>
): ResultWrapper<T> {
    return withContext(dispatcher) {
        try {
            val response = apiCall.invoke()
            if (response.isSuccessful) {
                val body = response.body()
                        ?: throw ApiException(
                                responseCode = response.code(),
                                serverMessage = response.message()
                        )
                ResultWrapper.Success(data = body)
            } else {
                ResultWrapper.Error(error = ApiException(
                        response.code(),
                        response.message()
                ))
            }
        } catch (e: Exception) {
            ResultWrapper.Error(error = e)
        }
    }
}
