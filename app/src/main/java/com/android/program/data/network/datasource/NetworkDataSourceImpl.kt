package com.android.program.data.network.datasource

import com.android.program.data.network.ApiService
import com.android.program.data.network.ResultWrapper
import com.android.program.data.network.model.ProgramResponse
import com.android.program.data.network.safeApiCall

class NetworkDataSourceImpl(private val apiService: ApiService) : NetworkDataSource {

    override suspend fun getProgram(serialNumber: String, borderId: Long, direction: Int)
            : ResultWrapper<ProgramResponse> {
        return safeApiCall {
            apiService.getProgram(serialNumber, borderId, direction)
        }
    }
}