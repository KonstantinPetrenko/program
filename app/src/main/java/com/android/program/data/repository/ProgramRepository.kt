package com.android.program.data.repository

import com.android.program.data.network.ResultWrapper
import com.android.program.ui.uimodel.ProgramUi

interface ProgramRepository {
    suspend fun getProgram(serialNumber: String, borderId: Long, direction: Int): ResultWrapper<ProgramUi>
}